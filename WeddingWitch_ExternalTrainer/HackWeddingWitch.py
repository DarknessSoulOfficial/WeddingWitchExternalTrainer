from uniref import WinUniRef

def GetWeddingWitchAsProcess():
    ref_wed = WinUniRef("Wedding Witch.exe")
    return ref_wed
def HackEXP(mult_exp : float):
    uniref_wedding = GetWeddingWitchAsProcess().find_class_in_image("Assembly-CSharp", "GlobalStat")
    uniref_wedding.find_field("ExpMultiplier").set_value(mult_exp) #Experience Multiplier Set :D
def GoldMult(gold_multipl : float):
    uni_wedding = GetWeddingWitchAsProcess().find_class_in_image("Assembly-CSharp", "GlobalStat")
    uni_wedding.find_field("GoldMultiplier").set_value(gold_multipl) #Gold Multiplier :D
