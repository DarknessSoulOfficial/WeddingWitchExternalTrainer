import dearpygui.dearpygui as dpg
import HackWeddingWitch
def GetTAG(tagname : str):
    return dpg.get_value(tagname)
def MainUI():
    dpg.create_context()

    with dpg.window(label="Wedding Witch Trainer by DarknessSoul", tag="WND_WEDDINGWITCH"):
        dpg.add_text("This Is My First Trainer For Game Wedding Witch... Enjoy!!! :3", color=[35, 0, 122, 255])
        dpg.add_input_float(label="EXP Multiplier", min_value=float(0), max_value=float(350000), tag="EXP_WEDFLOAT")
        dpg.add_button(label="Set EXP Multiplier", callback=lambda:HackWeddingWitch.HackEXP(GetTAG("EXP_WEDFLOAT")))
        dpg.add_input_float(label="GOLD MULTIPLIER", min_value=float(0), max_value=float(500000), tag="WEDDING_GOLDMULT")
        dpg.add_button(label="Set Gold Multiplier", callback=lambda:HackWeddingWitch.GoldMult(GetTAG("WEDDING_GOLDMULT")))
    dpg.create_viewport(title='Wedding Witch Trainer by DarknessSoul', width=750, height=750)
    dpg.setup_dearpygui()
    dpg.show_viewport()
    dpg.set_primary_window("WND_WEDDINGWITCH", True)
    dpg.start_dearpygui()
    dpg.destroy_context()
